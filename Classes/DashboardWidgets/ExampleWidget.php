<?php
namespace TYPO3\CMS\DashboardWidgetExample\DashboardWidgets;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */
class ExampleWidget implements \TYPO3\CMS\Dashboard\DashboardWidgetInterface {

	/**
	 * Example setting
	 *
	 * @var string
	 */
	protected $exampleSetting = NULL;

	/**
	 * Limit, If set, it will limit the results in the list.
	 *
	 * @var integer
	 */
	protected $cacheLifetime = NULL;

	/**
	 * Widget settings
	 *
	 * @var array
	 */
	protected $widget = NULL;

	/**
	 * Renders content
	 * @param TYPO3\CMS\Dashboard\Domain\Model\DashboardWidgetSetting $dashboardWidgetSetting
	 * @return string the rendered content
	 */
	public function render($dashboardWidgetSetting = NULL) {
		$this->initialize($dashboardWidgetSetting);

		$content = false;
		if ($this->cacheLifetime == NULL) {
			$content = $this->generateContent();
		} else {
			/** @var \TYPO3\CMS\Core\Cache\CacheManager $cacheManager */
			$cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager')->get('TYPO3\\CMS\\Core\\Cache\\CacheManager');
			$cacheIdentifier = 'dashboardWidget_' . (int)$dashboardWidgetSetting->getUid();

			if (TRUE === $cacheManager->hasCache('dashboard') && TRUE === $cacheManager->getCache('dashboard')->has($cacheIdentifier)) {
				$content = $cacheManager->getCache('dashboard')->get($cacheIdentifier);
			} else {
				$content = $this->generateContent();
				$cacheManager->getCache('dashboard')->set($cacheIdentifier, $content, array(), $this->cacheLifetime);
			}
			unset($cacheManager);
		}
		return $content;	
	}

	/*
	 * Initializes settings from flexform
	 * @param TYPO3\CMS\Dashboard\Domain\Model\DashboardWidgetSetting $dashboardWidgetSetting
	 * @return void
	 */
	private function initialize($dashboardWidgetSetting = NULL) {
		$flexformSettings = \TYPO3\CMS\Extbase\Service\FlexFormService::convertFlexFormContentToArray($dashboardWidgetSetting->getSettingsFlexform());
		$this->cacheLifetime = (int)$flexformSettings['settings']['cacheLifetime'] * 60;
		$this->exampleSetting = $flexformSettings['settings']['exampleSetting'];
		$this->widget = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['dashboard']['widgets'][$dashboardWidgetSetting->getWidgetIdentifier()];
	}

	/**
	 * Generates the content
	 * @return string
	 */
	private function generateContent() {
		$widgetTemplateName = $this->widget['template'];
		$exampleView = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager')->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
		$template = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($widgetTemplateName);
		$exampleView->setTemplatePathAndFilename($template);
		$exampleView->assign('currentdate', date());
		$exampleView->assign('cacheLifetime', $this->cacheLifetime);
		$exampleView->assign('exampleSetting', $this->exampleSetting);
		return $exampleView->render();
	}
}
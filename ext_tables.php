<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}
#################################
### REGISTER DASHBOARD WIDGET ###
#################################
# Add new list-item to widget_identifier select
$GLOBALS['TCA']['tx_dashboard_domain_model_dashboardwidgetsettings']['columns']['widget_identifier']['config']['items']['1434544153'] = array('Widget 1434544153', 1434544153);

# Add configuration for new widget
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['dashboard']['widgets']['1434544153'] = array(
		'name' => 'LLL:EXT:dashboard_widget_example/Resources/Private/Language/locallang.xlf:1434544153.name',
		'description' => 'LLL:EXT:dashboard_widget_example/Resources/Private/Language/locallang.xlf:1434544153.description',
		'icon' => 'EXT:dashboard_widget_example/Resources/Public/Icons/ExampleWidget.png',
       	'class' => 'TYPO3\\CMS\\DashboardWidgetExample\\DashboardWidgets\\ExampleWidget',
       	'template' => 'EXT:dashboard_widget_example/Resources/Private/Templates/DashboardWidgets/ExampleWidget.html',
);
# Add flexform to settings_flexform ds for widget
$TCA['tx_dashboard_domain_model_dashboardwidgetsettings']['columns']['settings_flexform']['config']['ds']['1434544153'] = 'FILE:EXT:dashboard_widget_example/Configuration/FlexForms/flexform_1434544153.xml';